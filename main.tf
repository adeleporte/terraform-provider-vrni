provider "vrni" {
  host      = "vrni.cpod-vcn2.az-demo.shwrfr.com"
  username  = "admin@local"
  password  = "VMware1!"
}

resource "vrni_application" "app1"{
  name      =  "Test3"

  tier {
    name                = "tiers-1"
    membershiptype      = "IP_RANGE"
    property            = "IP Address"
    value               = "1.0.168.144"
  }

  tier {
    name                = "tiers-2"
    membershiptype      = "SEARCH"
    property            = "name"
    value               = "'Jumpbox'"
  }

}

resource "vrni_threshold" "test3" {
  name        = "test3"
  application = "MyVCNApp"
}


