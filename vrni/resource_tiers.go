package vrni

import (
	//"fmt"
	"log"

	//"strconv"

	"context"

	"github.com/hashicorp/terraform/helper/schema"
	vrni "github.com/vmware/go-vmware-vrni"
)

func resourceTiers() *schema.Resource {
	return &schema.Resource{
		Create: resourceTiersCreate,
		Read:   resourceTiersRead,
		Update: resourceTiersUpdate,
		Delete: resourceTiersDelete,
		Importer: &schema.ResourceImporter{
			State: schema.ImportStatePassthrough,
		},

		Schema: map[string]*schema.Schema{
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"vmname": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"appid": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
		},
	}
}

func resourceTiersCreate(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************Application CREATE*****************************************************************")

	tierName := d.Get("name")
	appID := d.Get("appid").(string)
	client := m.(*vrni.APIClient)

	var vm_type vrni.AllEntityType
	vm_type = "VirtualMachine"

	criteria := vrni.GroupMembershipCriteria{
		MembershipType: "SearchMembershipCriteria",
		SearchMembershipCriteria: &vrni.SearchMembershipCriteria{
			EntityType: &vm_type,
			Filter:     "name = " + d.Get("vmname").(string),
		},
	}

	criterias := make([]vrni.GroupMembershipCriteria, 0)
	criterias = append(criterias, criteria)

	body := vrni.TierRequest{
		Name:                    tierName.(string),
		GroupMembershipCriteria: criterias,
	}

	res,_,_ := client.ApplicationsApi.AddTier(context.Background(), appID, body)

	d.SetId(res.EntityId)
	/*
		client := m.(*velocloud.APIClient)

		d.Set("activationkey", result.ActivationKey)
	*/
	return resourceTiersRead(d, m)
}

func resourceTiersRead(d *schema.ResourceData, m interface{}) error {
	log.Println("*********************************************** Application READ*****************************************************************")
	/*
		appName := d.Get("name")

		client := m.(*vrni.APIClient)

		results, _, _ := client.ApplicationsApi.ListApplications(context.Background(), nil)

		d.SetId("")
		for _, v := range results.Results {
			res, _, _ := client.ApplicationsApi.GetApplication(context.Background(), v.EntityId)
			log.Println(res.Name)
			log.Println(res.EntityType)
			log.Println(res.EntityId)
			if res.Name == appName {
				log.Println(res.Name)
				log.Println(res.EntityType)
				log.Println(res.EntityId)
				d.SetId(res.EntityId)
			}
		}
	*/
	return nil
}

func resourceTiersUpdate(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************Application UPDATE*****************************************************************")
	return nil
}

func resourceTiersDelete(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************Application DELETE*****************************************************************")

	return resourceTiersRead(d, m)
}
